//
//  FormView.swift
//  carmen
//
//  Created by Juan Daniel Peralta on 20/02/22.
//

import SwiftUI

struct FormView: View {
    
    var cardItem: CardItem
    @State var address: String = ""
    @State var urlPhoto: String = ""
    @State private var selectedOption = ""
    @State private var sourceType: UIImagePickerController.SourceType = .camera
    @State var selectedImage: UIImage?
    @State private var isImagePickerDisplay = false
    @State private var neightborStatus = false
    @State private var showNeighborAlert = false
    @State var showButton = true
    @State var enableSend = false
    @Environment(\.colorScheme) var scheme
        
    var body: some View {        
        VStack(alignment: .leading){
            //MARK: Title and welcome card
            HStack {
                Spacer()
                Text(cardItem.title == Constants.neighborCheck ? (neightborStatus ? cardItem.title : ("\(cardItem.title) - \(Constants.disabledOption)")) : cardItem.title)
                    .font(Font.custom(ConstantsFonts.PoppinsSemiBold, size: 18))
                    .multilineTextAlignment(.leading)
                    .frame(width: UIScreen.main.bounds.width * 0.85, alignment: .leading)
                Spacer()
            }.onTapGesture {
                hideKeyboard()
            }
            //MARK: Form for create the report model
            VStack {
                Form {
                    //Direction
                    Section(header: Text(Constants.helpDirection).font(Font.custom(ConstantsFonts.PoppinsRegular, size: 12))){
                        HStack {
                            Spacer()
                            TextField(Constants.direction, text: $address)
                                .font(Font.custom(ConstantsFonts.PoppinsRegular, size: 16))
                                .modifier(clearButton(text: $address))
                                //.frame(width: UIScreen.main.bounds.width * 0.85, alignment: .leading)
                            Spacer()
                        }
                    }
                    //Select Report Option
                    Section(header: Text(Constants.selectAOption).font(Font.custom(ConstantsFonts.PoppinsRegular, size: 12))) {
                        Menu {
                            Picker(selection: $selectedOption){
                                    ForEach(cardItem.cardsOptions){ report in
                                        Text(report.title).tag(report.title)
                                            .font(Font.custom(ConstantsFonts.PoppinsRegular, size: 12))
                                    }
                            } label: {}
                        } label: {
                            Text(selectedOption == "" ? Constants.selectAOption : selectedOption)
                                .font(Font.custom(ConstantsFonts.PoppinsRegular, size: 16))
                        }
                    }
                    //Take Photo
                    Section(header: Text(Constants.helpAddPicture).font(Font.custom(ConstantsFonts.PoppinsRegular, size: 12))) {
                        Button{
                            self.sourceType = .camera
                            self.isImagePickerDisplay.toggle()
                        } label: {
                            Text(selectedImage != nil ?  Constants.changePhoto : Constants.addPhoto)
                                .font(Font.custom(ConstantsFonts.PoppinsRegular, size: 16))
                        }
                    }
                    if selectedImage != nil {
                        Section(content: {}, footer: {
                            VStack {
                                Text(Constants.takenPhoto)
                                    .foregroundColor(Color(ConstantsColors.customGray))
                                    .font(Font.custom(ConstantsFonts.PoppinsRegular, size: 16))
                                HStack {
                                    Image(uiImage: selectedImage!)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .clipShape(Circle())
                                    .frame(width: 100, height: 100)
                                }.frame(width: UIScreen.main.bounds.width * 0.85, alignment: .center)
                            }.onTapGesture {
                                self.sourceType = .camera
                                self.isImagePickerDisplay.toggle()
                            }
                        })
                    } else {}
                }//End to Form
                
                //MARK: Button send
                if showButton {
                Button {
                    //TODO: Run the connection with Cloudinary and Airtable Function
                    Task {
                        do {
                            //MARK: show Alert with send clodinary
                            ServicesCloudinary().sendImageCloudinary(from: selectedImage!) { urlPhoto in
                                //MARK: show Alert with send airtable
                                
                                let records = records(records: [recordsItem(fields: fieldsItem(Reporte: self.cardItem.title, Direccion: address, Subcategoria: selectedOption, Foto: urlPhoto))])
                                
                                ServicesAlamofire().postInfoData(from: records, baseId: self.cardItem.baseId){ result in
                                    //MARK: show Alert with success airtable
                                    print("data save")
                                    print(result)
                                }
                            }
                            
                        } catch {
                            //MARK: show Alert with ERROR
                            print("Fetching images failed with error \(error)")
                        }
                    }
                } label: {
                        HStack {
                            Spacer()
                            HStack {
                                    Text(((address == "")||(selectedOption == "")||(selectedImage == nil)) ? Constants.unSend : Constants.send)
                                        .padding()
                                        .frame(width: UIScreen.main.bounds.width * 0.85, height: 40, alignment: .center)
                                        .foregroundColor(Color.white)
                                        .font(Font.custom(ConstantsFonts.PoppinsRegular, size: 15))
                                        .background(
                                            RoundedRectangle(cornerRadius: .infinity)
                                                .foregroundColor(((address == "")||(selectedOption == "")||(selectedImage == nil)) ? Color.gray : Color(ConstantsColors.PurpleCustom)))
                            }
                            Spacer()
                        }
                    }.disabled((address == "")||(selectedOption == "")||(selectedImage == nil))
                }
                Spacer()
                    .frame(height: 10)
            }.background(Color.white)
                .disabled(cardItem.title == Constants.neighborCheck ? (!neightborStatus) : false)
        }
        .background(Color(ConstantsColors.PurpleBackground))
        .toolbar{
            ToolbarItem(placement: .principal) {
                Text(Constants.sendReport)
                    .font(Font.custom(ConstantsFonts.PoppinsBold, size: 16))
                    .foregroundColor(Color(ConstantsColors.PurpleCustom))
            }
        }
        .sheet(isPresented: self.$isImagePickerDisplay) {
            ImagePickerView(selectedImage: self.$selectedImage, sourceType: self.sourceType)
        }
        .alert(Constants.onlyError, isPresented: $showNeighborAlert){
            Button(Constants.dismissAlert, role: .cancel, action: {})
        } message: {
            Text(Constants.messageNeighborInactive)
        }
        .onAppear{
            if cardItem.title == Constants.neighborCheck {
                let now = Calendar.current.component(.hour, from: Date())
                neightborStatus = neighborHour(hour: now)
                showNeighborAlert = !neightborStatus
                showButton = neightborStatus
                print(neightborStatus)
            }
        }
    }
}

extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    func neighborHour(hour: Int)->Bool {
        if hour > 7 && hour < 22 {
            //MARK: Validate hour of day for enabled the Neighbor Option                        
            return false
        } else {
            return true
        }
    }
}

struct clearButton: ViewModifier {
    //MARK: Check the textfield for add the X button for clean
    @Binding var text: String
    public func body(content: Content) -> some View {
        content
        if !text.isEmpty {
            Button(action: {
                self.text = ""
            }){
                Image(systemName: "x.circle.fill")
                    .foregroundColor(Color(uiColor: .opaqueSeparator))
            }
            .padding(.trailing, 1)
        }
    }
}
