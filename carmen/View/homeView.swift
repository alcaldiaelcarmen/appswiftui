

import SwiftUI

struct homeView: View {
    
    @Environment(\.colorScheme) var scheme
    @ObservedObject var allReports = ReadData()
    
    var body: some View {
        NavigationView{
            ZStack {
                VStack {
                    HeaderReportsView()
                    VStack {
                        Spacer()
                            .frame(height: 70)
                        HStack {
                            Text(Constants.selectReportText)
                                .foregroundColor(Color(ConstantsColors.customGray))
                                .font(Font.custom(ConstantsFonts.PoppinsSemiBold, size: 16))
                                .multilineTextAlignment(.leading)
                            Spacer()
                        }.frame(width: UIScreen.main.bounds.width * 0.85, alignment: .leading)
                        
                        ScrollView(.vertical,showsIndicators: true){
                                VStack(spacing: 10) {
                                    ForEach(allReports.tabsReports){ tab in
                                        // Menu Card View
                                        ReportCardView(tab:tab)
                                    }
                                }.padding(.bottom)
                                .frame(width: UIScreen.main.bounds.width * 0.85, alignment: .center)
                        }.frame(width: UIScreen.main.bounds.width)
                        Spacer()
                            .frame(height: 20)
                    }.background(Color.white, in: RoundedRectangle(cornerRadius: 30))
                }.background(Color(ConstantsColors.PurpleBackground))
                
                WelcomeCard()
                    .offset(y: -240)
                
            }
        }.preferredColorScheme(.light)
    }
}

struct HomeSwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        homeView()
    }
}

