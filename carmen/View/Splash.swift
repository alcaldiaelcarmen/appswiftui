//
//  Splash.swift
//  Alcaldia Más Cerca
//
//  Created by David Londoño on 20/03/22.
//

import SwiftUI

struct Splash: View {
    @ScaledMetric var size: CGFloat = 150
    @ObservedObject var networkManager = NetworkManager()
    @State private var presentingAlert = false
    @State private var expandEffect = false
    @State var timeRemaining = 3
    @State var timerState = false
    private let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        HStack {
            if timerState && networkManager.isConnected {
                homeView()
            }else{
                VStack(alignment: .center){
                    if !networkManager.isConnected {
                        VStack {
                            Spacer()
                                .frame(height: 45)
                            HStack {
                                Spacer()
                                Text(Constants.noNetworkConnection)
                                    .multilineTextAlignment(.center)
                                    .frame(width: UIScreen.main.bounds.width)
                                    .padding(.horizontal)
                                Spacer()
                            }
                            Spacer()
                                .frame(height: 10)
                        }.font(Font.custom(ConstantsFonts.PoppinsMedium, size: 20))
                            .background(Color.red)
                            .foregroundColor(Color.white)
                            .transition(AnyTransition.slide)
                            .animation(Animation.default, value: networkManager.isConnected)
                    }
                    Spacer()
                    Image(ConstansImages.logoAlcaldia)
                        .resizable()
                        .frame(width: size, height: size)
                        .opacity(expandEffect ? 1 : 0)
                        .scaleEffect(expandEffect ? 1 : 0.01)
                        .animation(Animation.easeInOut.delay(0.3), value: expandEffect)
                    HStack{
                        Spacer()
                        VStack (alignment: .center) {
                            Spacer()
                                .frame(height: 100)
                            Text(Constants.welcomeText)
                                .multilineTextAlignment(.center)
                            Spacer()
                                .frame(height: 100)
                            Text(Constants.majorSubname)
                                .multilineTextAlignment(.center)
                            Text(Constants.countryName)
                                .multilineTextAlignment(.center)                
                        }.foregroundColor(Color.white)
                            .padding()
                            .font(Font.custom(ConstantsFonts.PoppinsMedium, size: 20))
                        Spacer()
                    }
                    Spacer()
                }.ignoresSafeArea()
                    .background(Circle()
                        .fill(Color(ConstantsColors.PurpleCustom))
                        .scaleEffect(expandEffect ? 20 : 0.01)
                        .ignoresSafeArea())
                    .onAppear {
                        expandEffect = false
                        expandEffect.toggle()
                    }
                    .animation(Animation.easeInOut.delay(0.2), value: expandEffect)
                    .preferredColorScheme(.light)
                    .onReceive(timer) { _ in
                        if timeRemaining > 0 {
                            timeRemaining -= 1
                            timerState = false
                        } else {
                            timerState = true
                        }
                    }
            }
        }.transition(AnyTransition.opacity)
            .animation(Animation.easeIn, value: timerState && networkManager.isConnected)
    }
}

struct Splash_Previews: PreviewProvider {
    static var previews: some View {
        Splash()
    }
}
