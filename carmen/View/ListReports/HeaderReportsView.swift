//
//  HeaderReportsView.swift
//  carmen
//
//  Created by Juan Daniel Peralta on 21/03/22.
//

import SwiftUI

struct HeaderReportsView: View {
    var body: some View {
        VStack {
            Spacer()
                .frame(height: 20)
            HStack {
                Spacer()
                VStack (alignment: .leading) {
                    Text("\(Constants.governmentName)\n\(Constants.countryName)")
                        .foregroundColor(Color(ConstantsColors.PurpleCustom))
                        .font(Font.custom(ConstantsFonts.PoppinsBold, size: 22))
                        .multilineTextAlignment(.leading)
                    Spacer()
                        .frame(height: 5)
                    Text(Constants.needHelp)
                        .foregroundColor(Color.gray)
                        .font(Font.custom(ConstantsFonts.PoppinsRegular, size: 12))
                        .multilineTextAlignment(.leading)
                    Spacer()
                        .frame(height: 25)
                }.frame(width: UIScreen.main.bounds.width * 0.85, alignment: .leading)
                Spacer()
            }.background(Color(ConstantsColors.PurpleBackground))
            Spacer()
                .frame(height: 50)
        }
    }
}

struct HeaderReportsView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderReportsView()
    }
}
