//
//  ReportCardView.swift
//  carmen
//
//  Created by Juan Daniel Peralta on 21/03/22.
//

import SwiftUI

struct ReportCardView: View {
    let now = Calendar.current.component(.hour, from: Date())
    @State var hiddenTitles = true
    var tab: Tab    
    var body: some View {
        VStack(alignment: .leading, spacing: 0.3, content: {
            Text(tab.tab)
                .font(Font.custom(ConstantsFonts.PoppinsSemiBold, size: 25))
                .padding(.vertical)
            ForEach(tab.cards){ report in
                HStack(alignment: .top, spacing: 15) {
                   NavigationLink(destination: FormView(cardItem: report)) {
                        Image(report.image)
                            .resizable()
                            .scaledToFit()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 70, height: 70)
                            .padding()
                            .background(Color(ConstantsColors.PurpleBackground))
                       Spacer()
                           .frame(width: 15)
                        VStack(alignment: .leading, spacing: 10){
                            Text(report.title)
                                .foregroundColor(Color.black)
                                .font(Font.custom(ConstantsFonts.PoppinsSemiBold, size: 16))
                                .multilineTextAlignment(.leading)
                            Text(report.description)
                                .font(Font.custom(ConstantsFonts.PoppinsRegular, size: 14))
                                .foregroundColor(Color(ConstantsColors.customGray))
                                .multilineTextAlignment(.leading)
                        }
                    }.navigationBarBackButtonHidden(hiddenTitles)
                    .navigationBarHidden(hiddenTitles)                    
                    Spacer()
                }.background(Color(ConstantsColors.shadowColor), in: RoundedRectangle(cornerRadius: 15))
            }.frame(width: UIScreen.main.bounds.width * 0.85, height: 120)
        })
    }
}


