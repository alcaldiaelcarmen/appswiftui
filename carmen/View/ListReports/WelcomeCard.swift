//
//  WelcomeCard.swift
//  carmen
//
//  Created by David Londoño on 27/03/22.
//

import SwiftUI

struct WelcomeCard: View {
    var body: some View {
        VStack {
            HStack(alignment: .top, spacing: 15) {
                Image(ConstansImages.checkOk)
                    .resizable()
                    .scaledToFit()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 70, height: 70)
                    .padding()
                    .background(Color(ConstantsColors.PurpleCustom))
                VStack(alignment: .leading, spacing: 5){
                    Text(Constants.report)
                        .foregroundColor(Color(ConstantsColors.PurpleCustom))
                        .font(Font.custom(ConstantsFonts.PoppinsSemiBold, size: 16))
                        .multilineTextAlignment(.leading)
                        .padding(.top)
                    Text(Constants.reportWelcomeText)
                        .font(Font.custom(ConstantsFonts.PoppinsRegular, size: 14))
                        .foregroundColor(Color(ConstantsColors.customGray))
                        .multilineTextAlignment(.leading)
                }
                Spacer()
            }.background(Color(ConstantsColors.shadowColor), in: RoundedRectangle(cornerRadius: 15))
        }.frame(width: UIScreen.main.bounds.width * 0.85, height: 120)
    }
}

struct WelcomeCard_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeCard()
    }
}
