//
//  servicesAlamofire.swift
//  carmen
//
//  Created by Juan Daniel Peralta on 21/03/22.
//

import Foundation
import Alamofire

class ServicesAlamofire {
    
    func postInfoData(from recordingData : records, baseId: String, completionHandler: @escaping (Int) -> Void) {
        
        let headers : HTTPHeaders = [
            "Authorization": "Bearer \(ConstantsConfig.ApiKeyAirtable)",
            "Content-Type": "application/json",
        ]
        
        AF.request("https://api.airtable.com/v0/appNpSIapgRrh3sLl/principal",
                   method: .post,
                   parameters: recordingData,
                   encoder: JSONParameterEncoder.default, headers: headers)
        .validate(statusCode: 200..<300)
        .responseData{ response in
            switch response.result {
            case .success:
                print("Validation Successful")
                completionHandler(200)
            case .failure(_):
                completionHandler(500)
            }
        }
    }
}
