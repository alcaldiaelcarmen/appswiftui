//
//  servicesCamera.swift
//  carmen
//
//  Created by David Londoño on 16/05/22.
//

import UIKit
import SwiftUI

struct ImagePickerView: UIViewControllerRepresentable {
    @Binding var selectedImage: UIImage?
    @Environment(\.presentationMode) var isPresented
    var sourceType: UIImagePickerController.SourceType
    func makeUIViewController(context: Context) -> some UIViewController {
        let imagePicker = UIImagePickerController()        
        imagePicker.sourceType = self.sourceType
        imagePicker.delegate = context.coordinator//Confirming the delegate
        return imagePicker
    }
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) { }    
    //MARK: Connect the Coordinador class with this struct
    func makeCoordinator() -> Coordinator {
        return Coordinator(picker: self)
    }
}
