//
//  servicesCloudinary.swift
//  carmen
//
//  Created by Juan Daniel Peralta on 2/06/22.
//

import Foundation
import Cloudinary
import UIKit

class ServicesCloudinary {
    
    func sendImageCloudinary(from urlImage: UIImage, completionHandler: @escaping (String) -> Void) {
        
        let config = CLDConfiguration(cloudName: ConstantsConfig.CloudNameCLD, apiKey: ConstantsConfig.apiKeyCLD)
        let cloudinary = CLDCloudinary(configuration: config)
        cloudinary.createUploader().upload(data: urlImage.jpegData(compressionQuality: 50)!, uploadPreset: ConstantsConfig.uploadPresetCLD)
        { Progress in
            //progress
            print("enviando...")
        } completionHandler: { response, error in
            // complet resulr
            print(response?.url ?? "")
            completionHandler(response?.url ?? "No se pudo cargar")
        }
    }
}
