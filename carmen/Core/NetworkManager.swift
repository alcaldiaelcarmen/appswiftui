//
//  NetworkManager.swift
//  Alcaldia Más Cerca
//
//  Created by David Londoño on 21/03/22.
//

import Foundation
import Network

class NetworkManager: ObservableObject {
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "NetworkManager")
    @Published var isConnected = true
    init() {
        monitor.pathUpdateHandler = { path in
            DispatchQueue.main.sync {
                self.isConnected = path.status == .satisfied
            }
            print("Validando red...")
        }
        monitor.start(queue: queue)
    }    
}
