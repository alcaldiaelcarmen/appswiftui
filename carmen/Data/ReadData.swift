//
//  ReadData.swift
//  carmen
//
//  Created by Juan Daniel Peralta on 21/03/22.
//

import Foundation

class ReadData: ObservableObject {
    @Published var tabsReports = [Tab]()
    init(){
        loadData()
    }
    func loadData() {
        guard let url = Bundle.main.url(forResource: "reports", withExtension: "json") else {
            print("No se pudo leer el archivo")
            return
        }
        let data = try? Data(contentsOf: url)
        let tabsReports = try? JSONDecoder().decode([Tab].self, from: data!)
        self.tabsReports = tabsReports!
    }
}
