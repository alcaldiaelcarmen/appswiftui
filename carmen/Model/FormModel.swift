//
//  FormModel.swift
//  carmen
//
//  Created by Juan Daniel Peralta on 20/02/22.
//

import SwiftUI


struct records: Encodable {
    var records :[recordsItem]
}

struct recordsItem: Encodable {
    var fields : fieldsItem
}

//fields
struct fieldsItem: Encodable {
    var Reporte: String
    var Direccion: String
    var Subcategoria: String
    var Foto: String
}
        
