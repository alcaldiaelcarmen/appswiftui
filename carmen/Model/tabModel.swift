import Foundation

//MARK: Tab Model for every organization dependency
struct Tab: Codable, Identifiable {
    enum CodingKeys: CodingKey {
        case tab
        case cards
    }    
    var id = UUID().uuidString
    var tab : String
    var cards: [CardItem]
}
//MARK: Card Model for every tab
struct CardItem: Codable, Identifiable {
    
    enum CodingKeys: CodingKey {
        case title
        case description
        case image
        case cardsOptions
        case baseId
    }
    
    var id = UUID().uuidString
    var title: String
    var description: String
    var image: String
    var cardsOptions : [CardOptions]
    var baseId: String
}
//MARK: Options Model of Every Card
struct CardOptions: Codable, Identifiable {
    enum CodingKeys: CodingKey {
        case title
    }    
    var id = UUID().uuidString
    var title: String
}
