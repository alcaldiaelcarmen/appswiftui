//
//  Constants.swift
//  carmen
//
//  Created by David Londoño on 27/03/22.
//

import Foundation

class Constants {
    static let welcomeText = "Bienvenido"
    static let governmentName = "Alcaldía más cerca"
    static let countryName = "El Carmen de Viboral"
    static let majorSubname = "\"Más cerca,\nmás oportunidades\""
    static let onlyError = "Espera! ✋"
    static let noNetworkConnection = "Oops! 😕 \nParece que no tienes conexión a internet."
    static let selectReportText = "Selecciona el tipo de reporte:"
    static let needHelp = "¿Necesitas ayuda?"
    static let report = "Realiza tu reporte"
    static let reportWelcomeText = "Podrás realizar reportes según la categoría."
    static let sendReport = "Enviar Reporte"
    static let helpAddPicture = "Tome una foto que nos ayude a atender su reporte:"
    static let selectAOption = "Seleccione una opción:"
    static let helpDirection = "Ingrese la dirección del Reporte:"
    static let direction = "Dirección"
    static let changePhoto = "Cambiar foto"
    static let addPhoto = "Agrega una foto"
    static let takenPhoto = "Foto tomada:"
    static let send = "Enviar"
    static let dismissAlert = "OK"
    static let unSend = "Debes completar todos los campos"
    static let neighborCheck = "Vecinos ruidosos"
    static let disabledOption = "Opción No Disponible"
    static let messageNeighborInactive = "La opción de vecinos ruidosos sólo esta disponible entre las 10:00pm y las 6:00am."
}

class ConstantsFonts {
    static let PoppinsMedium = "Poppins-Medium"
    static let PoppinsRegular = "Poppins-Regular"
    static let PoppinsBold = "Poppins-Bold"
    static let PoppinsSemiBold = "Poppins-SemiBold"
}

class ConstantsColors {
    static let PurpleBackground = "PurpleBackground"
    static let PurpleCustom = "PurpleCustom"
    static let shadowColor = "shadowColor"
    static let customGray = "customGray"
}

class ConstansImages {
    static let logoAlcaldia = "LogoAlcaldia"
    static let checkOk = "checkOk"
    static let fireman = "fireman"
    static let light = "light"
    static let neighborSound = "neighborSound"
    static let police = "police"
    static let streetHole = "streetHole"
    static let transitGuard = "transitGuard"
    static let womenLine = "womenLine"
}

class ConstantsConfig {
    static let ApiKeyAirtable = "keyilrwQS61zccbmF"
    static let CloudNameCLD = "app-alcaldia"
    static let apiKeyCLD = "186146976467417"
    static let uploadPresetCLD = "ty8puguu"
}
